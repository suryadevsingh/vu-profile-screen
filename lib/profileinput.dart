
import 'package:flutter/material.dart';

class profileinput extends StatefulWidget {
  @override
  _profileinputState createState() => _profileinputState();
}

class _profileinputState extends State<profileinput> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: ScrollPhysics(parent: ScrollPhysics()),
      children: <Widget>[

        SizedBox(height: 15,),
        Container(

          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Seconodary email",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 35,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("johndeo@gmail.com",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),
        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("City",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("Toronto",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),
        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Province name ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("British ,Coumbia",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),
        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Gender ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("Male",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),
        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Postion titile",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("Head Coach/Recruiter",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),
        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Coaching level ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("IT",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),
        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Institute name ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("University of Calgary",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),

        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Institute team name ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("Dinos",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),

        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Institute city ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("Calgory",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),

        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Institute province ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("Alberta",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),

        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Gender coached ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("Female",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),

        SizedBox(height: 15,),
        Container(
          margin: EdgeInsets.only(left: 15,right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

              Text("Enter level ",style: TextStyle(color: Colors.grey,fontSize: 13),),
              Container(
                height: 36,
                width: 320,
                decoration: BoxDecoration(
                  color: Color(0xFFfbfbfb),
                  borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  border: Border.all(color: Color(0xFFf3f3f3),width: 1),

                ),
                child: Container(
                    margin: EdgeInsets.all(7),
                    child: Text("U12",style: TextStyle(color: Colors.grey))),
              ),
            ],
          ),
        ),

        SizedBox(height: 15,),

      ],
    );
  }
}
