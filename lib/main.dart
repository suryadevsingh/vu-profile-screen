
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:profilevu/profileinput.dart';

void main() => runApp(MaterialApp(home: MyApp()));



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: "Muli",
        primaryColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}


//class MyApp extends StatefulWidget {
//  @override
//  _MyAppState createState() => _MyAppState();
//}
//
//class _MyAppState extends State<MyApp> {
//
//  @override
//  Widget build(BuildContext context) {
//    return  MaterialApp(
//      theme: ThemeData(
//        fontFamily: "Muli",
//          primaryColor: Colors.white
//      ),
//      debugShowCheckedModeBanner: false,
//      home: Home()
//    );
//  }
//}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: DefaultTabController(
          length: 3,
          child: NestedScrollView(

            physics: ScrollPhysics(parent: BouncingScrollPhysics() ),
//              controller: ScrollController(keepScrollOffset: true),
            headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  floating: true,
                  snap: true,

//                    forceElevated: true,
                  titleSpacing: 0.0,
                  primary: false,
                  title: Text("Profile",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.w900),),
                  leading: IconButton(
                      icon: Icon(Icons.arrow_back_ios,size: 20,color: Colors.white,),
                      onPressed: (){
//                        Navigator.pop(context);

                      }
                  ),
                  actions: <Widget>[
                    GestureDetector(
                      onTap: (){

                      },
                      child: Container(
//                          height: 27,
//                          width: 27,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
//                                    color: Color(0xFF333333).withOpacity(0.5)
                        ),
                        child: Container(
                          height: 18,
                          width: 24,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage("assets/images/edit1.png",),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 15,),
                  ],
                  expandedHeight: 420.0,
//                    floating: true,
                  pinned: true,
//                   forceElevated: f,
                  flexibleSpace: FlexibleSpaceBar(
                    collapseMode: CollapseMode.parallax,
//                      stretchModes: [
//                        StretchMode.fadeTitle,
//                        StretchMode.fadeTitle,
//                        StretchMode.blurBackground,
//                        StretchMode.zoomBackground
//                      ],
                    centerTitle: true,
                    title:
                    Stack(
//                        fit: StackFit.loose,
                      overflow: Overflow.visible,
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        Positioned(
                          top: 227,
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: 80,
                                height: 80,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: AssetImage("assets/images/face.jpg")
                                    ),
                                    border: Border.all(
                                      color: Colors.white,
                                      width: 4.0,
                                    )
                                ),
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          top: 290,
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 14,),
                              Center(child: Text("John duo",style: TextStyle(fontSize: 14,color: Colors.black,fontWeight: FontWeight.bold),)),
                              SizedBox(height: 2,),
                              Center(child: Text(" Instructor,coach,Consultant",style: TextStyle(fontSize: 10,color: Colors.grey[500],fontWeight: FontWeight.normal),)),
                              SizedBox(height: 3,),
                              Center(child: Text(" Alberta , British columbia",style: TextStyle(fontSize: 10,color: Colors.grey[500],fontWeight: FontWeight.normal),)),

                              SizedBox(height: 10,),

                            ],
                          ),
                        ),

                        Positioned(
                          top: 355,
                          child: Row(children: <Widget>[

                            SizedBox(width: 0,),
                            Column(
                              children: <Widget>[
                                Text("146k",style: TextStyle(color: Colors.black,fontSize: 12,fontWeight: FontWeight.bold),),
                                Text("blogs",style: TextStyle(color: Colors.grey,fontSize: 10),),
                              ],
                            ),

                            SizedBox(width: 35,),
//                      VerticalDivider(color: Colors.black,thickness: 5,width: 20,),
                            Container(
                              width: 1,
                              height: 20,
                              color: Colors.grey,
                            ),
                            SizedBox(width:18 ,),
                            Column(
                              children: <Widget>[
                                Text("700",style: TextStyle(color: Colors.black,fontSize: 12,fontWeight: FontWeight.bold),),
                                Text("Connections",style: TextStyle(color: Colors.grey,fontSize: 10),),
                              ],
                            ),
                            SizedBox(width:18,),
                            Container(
                              width: 1,
                              height: 20,
                              color: Colors.grey,
                            ),
                            SizedBox(width:35 ,),
                            Column(
                              children: <Widget>[
                                Text("700",style: TextStyle(color: Colors.black,fontSize: 12,fontWeight: FontWeight.bold),),
                                Text("Groups",style: TextStyle(color: Colors.grey,fontSize: 10),),
                              ],
                            ),
                          ],
                          ),
                        ),


                      ],
                    ),
                    background: Stack(
                      overflow: Overflow.visible,
                      children: <Widget>[
                        Container(
                          height: 180,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: AssetImage("assets/images/voll1.jpg"),

                            ),
                          ),
                        ),
//                          Positioned(
//                            top: 10,
//                            left: 0,
//                            child: IconButton(
//                                icon: Icon(Icons.arrow_back_ios,size: 20,color: Colors.white,),
//                                onPressed: (){
////                        Navigator.pop(context);
//
//                                }
//                            ),
//                          ),

//                          Positioned(right: 13,
//                            top: 22,
//                            child: GestureDetector(
//                              onTap: (){
//
//                              },
//                              child: Container(
//                                height: 27,
//                                width: 27,
//                                decoration: BoxDecoration(
//                                  shape: BoxShape.circle,
////                                    color: Color(0xFF333333).withOpacity(0.5)
//                                ),
//                                child: Container(
////                                  height: 28,
////                                  width: 28,
//                                  decoration: BoxDecoration(
//                                    image: DecorationImage(
//                                      fit: BoxFit.fill,
//                                      image: AssetImage("assets/images/edit1.png"),
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            ),
//                          ),
                        Positioned(
                          right: 10,
                          top: 150,
//                    bottom: 100,
                          child: Container(
                            height: 20,
                            width: 110,
                            child: FAProgressBar(
                              backgroundColor: Colors.grey[300],
                              progressColor: Colors.red,
                              currentValue: 60,

                              displayText: "%",
                            ),
                          ),

                        ),

//                          Positioned(
//                              top: 20,
//                              left: 35,
//                              child:
//                              Text("Profile",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.w900),)
//
//                          ),


                      ],
                    ),
                  ),
                  bottom: TabBar(
                      indicatorColor: Colors.red,
                      labelColor: Colors.black87,
                      unselectedLabelColor: Colors.grey,
                      labelStyle: TextStyle(fontSize: 18),
                      indicatorSize: TabBarIndicatorSize.label,
                      tabs: [
                        Tab(
                          child: FittedBox(
                            fit:BoxFit.fitWidth,
                            child: Text("Activities",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Tab(
                          child: FittedBox(
                            fit:BoxFit.fitWidth,
                            child: Text("Personal",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Tab(
                          child: FittedBox(
                            fit:BoxFit.fitWidth,
                            child: Text("Following",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ]
                  ),
                ),

              ];
            },
            body: TabBarView(children: [
              Container(
                  color: Colors.white,
                  child: profileinput()),
              Container(
                  color: Colors.white,
                  child: profileinput()),
              Container(
                  color: Colors.white,
                  child: profileinput()),


            ]),
          ),
        ),
        bottomNavigationBar: BottomNavyBar(
          selectedIndex: currentIndex,
          onItemSelected: (index) => setState(() {
            currentIndex = index;
          }),
          showElevation: true,
          items: [
            BottomNavyBarItem(
              icon: Icon(Icons.home),
              activeColor: Colors.red,
              inactiveColor: Colors.grey,
              title: Text("home"),
            ),
            BottomNavyBarItem(
              icon: Icon(Icons.group),
              activeColor: Colors.red,
              inactiveColor: Colors.grey,
              title: Text("group"),
            ),
            BottomNavyBarItem(
              icon: Icon(Icons.add_box),
              title: Text("create"),
              activeColor: Colors.red,
              inactiveColor: Colors.grey,
            ),
            BottomNavyBarItem(
              icon: Icon(Icons.group_work),
              activeColor: Colors.red,
              inactiveColor: Colors.grey,
              title: Text("group work"),
            ),
            BottomNavyBarItem(
              icon: Icon(Icons.face),
              activeColor: Colors.red,
              inactiveColor: Colors.grey,
              title: Text("profile"),
            ),
          ],
        ),
      ),
    );
  }
}
